require'nvim-treesitter.configs'.setup {
  ensure_installed = { "c", "cpp", "lua", "rust", "python", "fish", "latex"},
  highlight = {
    enable = true,
  },
}

