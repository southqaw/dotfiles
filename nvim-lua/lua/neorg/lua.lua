require('neorg').setup {
    load = {
        ["core.defaults"] = {},
        ["core.norg.dirman"] = {
            config = {
                workspaces = {
                    work = "~/Notes/work",
                    meetings = "~/Notes/work/meetings",
                    home = "~/Notes/home",
                }
            }
        },
        ["core.norg.completion"] = {
            config = {
                engine = 'nvim-cmp',
            }
        },
--        ["core.gtd.base"] = {
--            config = {
--                workspace = "work",
--            },
--        },
        ["core.norg.concealer"] = {
            config = {
                icons = {
                    todo = {
                        undone = {
                            enabled = false,
                        },
                    },
                },
            },
        },
        ["core.norg.esupports.metagen"] = {
            config = {
                type = "auto",
            },
        }
    }
}

