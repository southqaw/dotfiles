-- Install packer
local execute = vim.api.nvim_command

local install_path = vim.fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'

if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
    execute('!git clone https://github.com/wbthomason/packer.nvim '.. install_path)
end

vim.api.nvim_exec([[
augroup Packer
autocmd!
autocmd BufWritePost init.lua PackerCompile
augroup end
]], false)

local use = require('packer').use
require('packer').startup(function()
    use 'wbthomason/packer.nvim'       -- Package manager
    use 'tpope/vim-commentary'         -- "gc" to comment visual regions/lines
    use 'tpope/vim-abolish'
    use 'ludovicchabant/vim-gutentags' -- Automatic tags management
    -- UI to select things (files, grep results, open buffers...)
    use {'nvim-telescope/telescope.nvim', requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}} }
    -- use 'ishan9299/nvim-solarized-lua' -- Lua port of vim-solarized8
    -- use 'shaunsingh/solarized.nvim'  -- Light only for now
    use {"ellisonleao/gruvbox.nvim", requires = {"rktjmp/lush.nvim"}}
    -- use 'itchyny/lightline.vim'        -- Fancier statusline
    -- Add indentation guides even on blank lines
    use 'lukas-reineke/indent-blankline.nvim'
    -- Add git related info in the signs columns and popups
    use {'lewis6991/gitsigns.nvim', requires = {'nvim-lua/plenary.nvim'} }
    use 'neovim/nvim-lspconfig'        -- Collection of configurations for built-in LSP client
    use 'hrsh7th/cmp-nvim-lsp'
    use 'L3MON4D3/LuaSnip'
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-path'
    use 'hrsh7th/cmp-cmdline'
    use 'saadparwaiz1/cmp_luasnip'
    use 'hrsh7th/nvim-cmp'
    use {"nvim-neorg/neorg", requires = "nvim-lua/plenary.nvim"}
    use 'folke/lsp-trouble.nvim'
    use 'axelf4/vim-strip-trailing-whitespace'
    use 'lambdalisue/suda.vim'
    use 'nvim-treesitter/nvim-treesitter'
    use 'ryanoasis/vim-devicons'
    use 'kyazdani42/nvim-web-devicons'
    use {'nvim-lualine/lualine.nvim', requires = {'kyazdani42/nvim-web-devicons', opt = true} }
    use {'akinsho/bufferline.nvim', requires = 'kyazdani42/nvim-web-devicons'}
    use 'nvim-lua/lsp_extensions.nvim'
    use 'numtostr/FTerm.nvim'
    use {
	"luukvbaal/stabilize.nvim",
	config = function() require("stabilize").setup() end
    }
    use 'khaveesh/vim-fish-syntax'
    use 'dstein64/vim-startuptime'
    use 'kergoth/vim-bitbake'
    use 'Maan2003/lsp_lines.nvim'
    use 'lvimuser/lsp-inlayhints.nvim'
end)

--Incremental live completion
vim.o.inccommand = "nosplit"

--Set highlight on search
vim.o.hlsearch = false
vim.o.incsearch = true

--Make line numbers default
vim.wo.number = true
vim.wo.rnu = true
vim.api.nvim_exec([[
augroup numbertoggle
autocmd!
autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END
]], false)
--Do not save when switching buffers
vim.o.hidden = true

--Enable mouse mode
vim.o.mouse = "a"

--Enable break indent
vim.o.breakindent = true

--Enable auto indent
vim.o.autoindent = true

--Indent settings
vim.o.tabstop = 4
vim.o.shiftwidth = 4
vim.o.softtabstop = 4
vim.o.expandtab = true
vim.o.smartindent = true
vim.o.smarttab = true

--Save undo history
vim.cmd[[set undofile]]

--Case insensitive searching UNLESS /C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

--Decrease update time
vim.o.updatetime = 250
vim.wo.signcolumn="yes"

--Set colorscheme (order is important here)
vim.o.termguicolors = true
-- if vim.fn.has('gui_running') == 0 then
--     vim.g.solarized_termtrans = 0
-- else
--     vim.g.solarized_termtrans = 1
-- end
vim.o.background = "dark"
vim.cmd[[colorscheme gruvbox]]

--Remap space as leader key
vim.api.nvim_set_keymap('', '<Space>', '<Nop>', { noremap = true, silent=true})
vim.g.mapleader = " "
vim.g.maplocalleader = " "

--Remap for dealing with word wrap
vim.api.nvim_set_keymap('n', 'k', "v:count == 0 ? 'gk' : 'k'", { noremap=true, expr = true, silent = true})
vim.api.nvim_set_keymap('n', 'j', "v:count == 0 ? 'gj' : 'j'", {noremap= true, expr = true, silent = true})

--Remap escape to leave terminal mode
vim.api.nvim_exec([[
augroup Terminal
autocmd!
au TermOpen * tnoremap <buffer> <Esc> <c-\><c-n>
au TermOpen * set nonu norelativenumber
augroup end
]], false)

--Add map to enter paste mode
vim.o.pastetoggle="<F3>"

--Map blankline
vim.g.indent_blankline_char = "┊"
vim.g.indent_blankline_filetype_exclude = { 'help', 'packer' }
vim.g.indent_blankline_buftype_exclude = { 'terminal', 'nofile'}
vim.g.indent_blankline_char_highlight = 'LineNr'
vim.g.indent_blankline_use_treesitter = true

-- Toggle to disable mouse mode and indentlines for easier paste
ToggleMouse = function()
    if vim.o.mouse == 'a' then
        vim.cmd[[IndentBlanklineDisable]]
        vim.wo.signcolumn='no'
        vim.o.mouse = 'v'
        vim.wo.number = false
        vim.wo.rnu = false
        print("Mouse disabled")
    else
        vim.cmd[[IndentBlanklineEnable]]
        vim.wo.signcolumn='yes'
        vim.o.mouse = 'a'
        vim.wo.number = true
        vim.wo.rnu = true
        print("Mouse enabled")
    end
end

vim.api.nvim_set_keymap('n', '<F10>', '<cmd>lua ToggleMouse()<cr>', { noremap = true })

-- Change preview window location
vim.g.splitbelow = true

-- Highlight on yank
vim.api.nvim_exec([[
augroup YankHighlight
autocmd!
autocmd TextYankPost * silent! lua vim.highlight.on_yank()
augroup end
]], false)

-- Y yank until the end of line
vim.api.nvim_set_keymap('n', 'Y', 'y$', { noremap = true})
--
-- Map :Format to vim.lsp.buf.formatting()
vim.cmd([[ command! Format execute 'lua vim.lsp.buf.formatting()' ]])

-- Set completeopt to have a better completion experience
vim.o.completeopt="menu,menuone,noselect"

-- Set suda enable automatically
vim.g.suda_smart_edit = 1

-- Explicit Python3 executable
vim.g.python3_host_prog = 'python3'

-- LSP Diagnostics
-- vim.g.diagnostics_show_sign = 1
vim.g.diagnostics_auto_popup_while_jump = 1
vim.g.diagnostics_insert_delay = 1
vim.diagnostic.config({
    virtual_text = false,  -- True for no lsp_lines
    signs = false,  -- True for no lsp_lines
    float = { scope = "line", border = "single" },
    underline = false,  -- True for no lsp_lines
    update_in_insert = true,
    severity_sort = false,
})

-- Fish Shell Fix
vim.opt.shell = "/bin/bash"

--TMUX
vim.api.nvim_exec([[
autocmd BufReadPost,FileReadPost,BufNewFile,BufEnter * call system("tmux rename-window " . expand("%:p:h:t")."/".expand("%:t"))
]], false)
vim.api.nvim_exec([[
autocmd VimLeave * call system("tmux rename-window fish")
]], false)



-- LSP Extensions
vim.api.nvim_exec([[
autocmd CursorMoved,InsertLeave,BufEnter,BufWinEnter,TabEnter,BufWritePost *.rs lua require'lsp_extensions'.inlay_hints{ prefix = '', highlight = "Comment" }
]], false)

require('lsp.lua')
require('treesitter.lua')
require('lualine.lua')
--require'lualine'.setup()
require('bufferline.lua')
require('gitsigns.lua')
require('lsp-trouble.lua')
require('neorg.lua')
require('telescope.lua')
require('cmp.lua')
require('FTerm.lua')
require('lsp_lines').setup()
require("lsp-inlayhints").setup()
