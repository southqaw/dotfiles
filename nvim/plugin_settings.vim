"Let vim-gitgutter do its thing on large files
let g:gitgutter_max_signs=1000
let g:gitgutter_map_keys = 0
let g:gitgutter_highlight_linenrs = 1
let g:gitgutter_preview_win_floating = 1
let g:gitgutter_use_location_list = 0
let g:gitgutter_sign_added = '+a'
let g:gitgutter_sign_modified = '+m'
let g:gitgutter_sign_removed = '-r'
let g:gitgutter_sign_removed_first_line = '^^'
let g:gitgutter_sign_modified_removed = 'mr'

" Neovim LSP Diagnostics
let g:diagnostic_show_sign = 1
let g:diagnostic_auto_popup_while_jump = 1
let g:diagnostic_insert_delay = 1

" For nvim-completion
"let g:completion_enable_snippet = 'UltiSnips'
let g:completion_enable_auto_popup = 1
let g:completion_auto_change_source = 1
let g:completion_chain_complete_list = {
  \ 'c': [
  \    {'complete_items': ['lsp', 'snippet']},
  \    {'mode': 'keyn'},
  \    {'mode': 'tags'},
  \    {'mode': '<c-p>'},
  \    {'mode': '<c-n>'}
  \],
  \ 'default': [
  \    {'complete_items': ['lsp', 'snippet']},
  \    {'mode': 'keyn'},
  \    {'mode': '<c-p>'},
  \    {'mode': '<c-n>'},
  \],
  \}
" Avoid showing message extra message when using completion
set shortmess+=c

" Visualize diagnostics
let g:diagnostic_enable_virtual_text = 1
let g:diagnostic_trimmed_virtual_text = '40'
" Don't show diagnostics while in insert mode
let g:diagnostic_insert_delay = 1

let g:solarized_termtrans=1

let g:suda_smart_edit = 1

let g:tagbar_position='rightbelow'
let g:tagbar_height=20

" Automatically detect style file and apply style to formatting
let g:clang_format#detect_style_file = 1
" Toggle quickfix/location list
let g:toggle_list_no_mappings = 1

let g:indent_blankline_use_treesitter = v:true
let g:indent_blankline_char = '┆' "'│'

" C/CPP
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_posix_standard = 1
let g:cpp_experimental_template_highlight = 1
let g:cpp_concepts_highlight = 1

lua require 'lsp.lua'
lua require 'treesitter.lua'
lua require 'lualine.lua'
lua require 'bufferline.lua'
lua require 'gitsigns.lua'
lua require 'lsp-trouble.lua'
exec 'source' '~/.config/nvim/vimscript/which-key.vim'
