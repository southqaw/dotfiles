" Set up leaders 
let mapleader="\<SPACE>"
let maplocalleader=","

syntax enable
filetype plugin indent on

function! LspStatus() abort
  if luaeval('#vim.lsp.buf_get_clients() > 0')
    return luaeval("require('lsp-status').status()")
  endif

  return ''
endfunction

set showtabline=2
set guioptions-=e
set laststatus=2        " Always show status bar
set mouse=nv
set nu
set rnu
set encoding=utf-8
set autoindent          " Copy indent from current line when starting a new line
set undofile            " Enable undo persistence across sessions
set nofoldenable        " Open folds by default
set ignorecase              " Make searching case insensitive
set smartcase               " ... unless the query has capital letters.
set completeopt-=menu
set completeopt-=longest    " Don't insert the longest common text
set completeopt-=preview    " Hide the documentation preview window
set completeopt+=menuone    " Show the completions UI even with only 1 item
set completeopt+=noinsert   " Don't insert text automatically
set completeopt+=noselect   " Do not select a match in the menu
set shortmess+=c           " Avoid showing extra messages when using completion
set updatetime=100      " Let plugins show effects after 100ms
set fileformat=unix
set backspace=indent,eol,start
set scrolloff=3            " Show next 3 lines while scrolling.
set sidescrolloff=5        " Show next 5 columns while side-scrolling.

" Wild menu
set wildmenu
set wildmode=longest:full,full
set wildoptions=pum
set pumblend=30
set wildignore=*.o,*.obj,*~,*.exe,*.a,*.pdb,*.lib
set wildignore+=__pycache__,.stversions,*.spl,*.out,%*
set wildignore+=*.so,*.dll,*.swp,*.egg,*.jar,*.class,*.pyc,*.pyo,*.bin,*.dex
set wildignore+=*.zip,*.7z,*.rar,*.gz,*.tar,*.gzip,*.bz2,*.tgz,*.xz
set wildignore+=*DS_Store*,*.ipch
set wildignore+=*.gem
set wildignore+=*.png,*.jpg,*.gif,*.bmp,*.tga,*.pcx,*.ppm,*.img,*.iso
set wildignore+=*.so,*.swp,*.zip,*/.Trash/**,*.pdf,*.dmg,*/.rbenv/**
set wildignore+=*/.nx/**,*.app,*.git,.git
set wildignore+=*.wav,*.mp3,*.ogg,*.pcm
set wildignore+=*.mht,*.suo,*.sdf,*.jnlp
set wildignore+=*.chm,*.epub,*.pdf,*.mobi,*.ttf
set wildignore+=*.mp4,*.avi,*.flv,*.mov,*.mkv,*.swf,*.swc
set wildignore+=*.ppt,*.pptx,*.docx,*.xlt,*.xls,*.xlsx,*.odt,*.wps
set wildignore+=*.msi,*.crx,*.deb,*.vfd,*.apk,*.ipa,*.bin,*.msu
set wildignore+=*.gba,*.sfc,*.078,*.nds,*.smd,*.smc
set wildignore+=*.linux2,*.win32,*.darwin,*.freebsd,*.linux,*.android

" We do this to prevent the loading of the system fzf.vim plugin. This is
" present at least on Arch/Manjaro
set rtp-=/usr/share/vim/vimfiles

" Colorscheme stuff
set t_8f=\[[38;2;%lu;%lu;%lum
set t_8b=\[[48;2;%lu;%lu;%lum
set termguicolors

set background=dark
colorscheme solarized8
hi! Normal ctermbg=NONE guibg=NONE
hi! NonText ctermbg=NONE guibg=NONE

