" Define prefix dictionary
let g:which_key_map =  {}

let g:which_key_map.1 = 'which_key_ignore'
let g:which_key_map.2 = 'which_key_ignore'
let g:which_key_map.3 = 'which_key_ignore'
let g:which_key_map.4 = 'which_key_ignore'
let g:which_key_map.5 = 'which_key_ignore'
let g:which_key_map.6 = 'which_key_ignore'
let g:which_key_map.7 = 'which_key_ignore'
let g:which_key_map.8 = 'which_key_ignore'
let g:which_key_map.9 = 'which_key_ignore'

let g:which_key_map.a = { 
      \ 'name' : '+lsp-actions',
      \ 'f' : 'Code Action',
      \ 'h' : 'Hover',
      \ 'i' : 'Incoming Calls',
      \ 'o' : 'Outgoing Calls',
      \ 'r' : 'Rename',
      \ '=' : 'Formatting',
      \}

let g:which_key_map.b = {
      \ 'name' : '+buffer',
      \ 'f': 'First',
      \ 'l': 'Last',
      \ 'n': 'Next',
      \ 'p': 'Previous',
      \ 'd': 'Close',
      \}

let g:which_key_map.c = {
      \ 'name' : '+change',
      \ '.': 'S&R word',
      \ ',': 'S&R word under cursor',
      \}

let g:which_key_map.D = {
      \ 'name' : '+diag-wrap',
      \ 'n': 'Next',
      \ 'p': 'Previous',
      \}

let g:which_key_map.d = {
      \ 'name' : '+diagnostics',
      \ 'n': 'Next',
      \ 'p': 'Previous',
      \}

let g:which_key_map.g = {
      \ 'name' : '+lsp-buf',
      \ 'D': 'Declaration',
      \ 'd': 'Definition',
      \ 'i': 'Implementation',
      \ 'r': 'References',
      \ 'R': 'References (Trouble)',
      \ 's': 'Signature Help',
      \ 't': 'Type Definition',
      \ 'W': 'Workspace Symbol',
      \ 'w': 'Document Symbol',
      \}

let g:which_key_map.h = {
      \ 'name' : '+gitsigns',
      \ 'b': 'Blame line',
      \ 'p': 'Preview hunk',
      \ 'r': 'Reset hunk',
      \ 'R': 'Reset buffer',
      \ 's': 'Stage hunk',
      \ 'u': 'Undo stage hunk',
      \}

let g:which_key_map.x = {
      \ 'name' : '+trouble',
      \ 'x': 'Toggle',
      \ 'w': 'Workspace Diagnostics',
      \ 'd': 'Document Diagnostics',
      \ 'q': 'Quickfix',
      \ 'l': 'Loclist',
      \}


call which_key#register('<Space>', "g:which_key_map")

nnoremap <silent> <leader> :<C-U>WhichKey '<Space>'<CR>
nnoremap <silent> <localleader> :<C-U>WhichKey ','<CR>

