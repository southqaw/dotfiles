call plug#begin()

" Collection of common configurations for the Nvim LSP client
Plug 'neovim/nvim-lspconfig'
" Extensions to built-in LSP, for example, providing type inlay hints
Plug 'tjdevries/lsp_extensions.nvim'
" Autocompletion framework for built-in LSP
Plug 'nvim-lua/completion-nvim'
" Diagnostic navigation and settings for built-in LSP
Plug 'nvim-lua/lsp-status.nvim'
Plug 'RishabhRD/popfix'
Plug 'RishabhRD/nvim-lsputils'
Plug 'onsails/lspkind-nvim'
Plug 'folke/lsp-trouble.nvim'
" ColorSchemes
Plug 'lifepillar/vim-solarized8'
" Remove extraneous whitespace when edit mode is exited
Plug 'axelf4/vim-strip-trailing-whitespace'
" Tim pope essentials
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-sleuth'
"Git Stuff
Plug 'nvim-lua/plenary.nvim'
Plug 'lewis6991/gitsigns.nvim'
Plug 'tpope/vim-fugitive'
" Take care of sudo 
Plug 'lambdalisue/suda.vim'
" Smooth scrolling
Plug 'psliwka/vim-smoothie'
" Scrollbar
Plug 'Xuyuanp/scrollbar.nvim'
" For C, Lua and Rust 
Plug 'nvim-treesitter/nvim-treesitter'
" Show leader key bindings
Plug 'liuchengxu/vim-which-key'
" Other syntax highlighting support
Plug 'georgewitteman/vim-fish'
Plug 'elzr/vim-json'
Plug 'lifepillar/pgsql.vim'
Plug 'mtdl9/vim-log-highlighting'
Plug 'martinda/Jenkinsfile-vim-syntax'
" Ctags
Plug 'majutsushi/tagbar'
Plug 'ludovicchabant/vim-gutentags'
Plug 'scrooloose/nerdtree'
Plug 'pseewald/nerdtree-tagbar-combined'
Plug 'ryanoasis/vim-devicons'
" Status Line
Plug 'kyazdani42/nvim-web-devicons'
Plug 'hoob3rt/lualine.nvim'
Plug 'akinsho/nvim-bufferline.lua'
" Quickfix
Plug 'ronakg/quickr-cscope.vim'
Plug 'milkypostman/vim-togglelist'
Plug 'yssl/QFEnter'
Plug 'chengzeyi/fzf-preview.vim'
" Indent Guide
Plug 'lukas-reineke/indent-blankline.nvim', { 'branch': 'lua' }

call plug#end()

let g:vim_home = get(g:, 'vim_home', expand('~/.config/nvim/'))
let config_list = [
      \ 'autocmd.vim',
      \ 'config.vim',
      \ 'plugin_settings.vim',
      \ 'keymappings.vim',
      \]
for files in config_list
  for f in glob(g:vim_home.files, 1, 1)
    exec 'source' f
  endfor
endfor

