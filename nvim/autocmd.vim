" Show diagnostic popup on cursor hold
autocmd CursorHold * lua vim.lsp.diagnostic.show_line_diagnostics()

autocmd CursorMoved,InsertLeave,BufEnter,BufWinEnter,TabEnter,BufWritePost *
\ lua require'lsp_extensions'.inlay_hints{ prefix = '', highlight = "Comment" }

augroup LuaHighlight
  autocmd!
  autocmd TextYankPost * silent! lua require'vim.highlight'.on_yank("IncSearch", 2000)
augroup END

augroup Scrollbar
  autocmd!
  autocmd BufEnter    * silent! lua require('scrollbar').show()
  autocmd BufLeave    * silent! lua require('scrollbar').clear()

  autocmd CursorMoved * silent! lua require('scrollbar').show()
  autocmd VimResized  * silent! lua require('scrollbar').show()

  autocmd FocusGained * silent! lua require('scrollbar').show()
  autocmd FocusLost   * silent! lua require('scrollbar').clear()
augroup END

augroup ToggleSearchHighlighting
  autocmd!
  autocmd InsertEnter * setlocal nohlsearch
augroup END

augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

autocmd BufReadPost,FileReadPost,BufNewFile * call system("tmux rename-window " . expand("%"))
autocmd VimLeave * call system("tmux rename-window fish")
