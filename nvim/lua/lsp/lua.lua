local nvim_lsp = require('lspconfig')
-- local diagnostic = require('diagnostic')
local completion = require('completion')
local lsp_status = require('lsp-status')

require('lspkind').init()

vim.lsp.handlers['textDocument/codeAction'] = require'lsputil.codeAction'.code_action_handler
vim.lsp.handlers['textDocument/references'] = require'lsputil.locations'.references_handler
vim.lsp.handlers['textDocument/definition'] = require'lsputil.locations'.definition_handler
vim.lsp.handlers['textDocument/declaration'] = require'lsputil.locations'.declaration_handler
vim.lsp.handlers['textDocument/typeDefinition'] = require'lsputil.locations'.typeDefinition_handler
vim.lsp.handlers['textDocument/implementation'] = require'lsputil.locations'.implementation_handler
vim.lsp.handlers['textDocument/documentSymbol'] = require'lsputil.symbols'.document_handler
vim.lsp.handlers['workspace/symbol'] = require'lsputil.symbols'.workspace_handler

local map = function(type, key, value)
	vim.api.nvim_buf_set_keymap(0,type,key,value,{noremap = true, silent = true});
end

local on_attach = function(client, bufnr)
  print("LSP started.");
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
  -- diagnostic.on_attach(client, bufnr)
  completion.on_attach(client, bufnr)
  lsp_status.on_attach(client, bufnr)

  map('n','<leader>gD','<cmd>lua vim.lsp.buf.declaration()<CR>')
  map('n','<leader>gd','<cmd>lua vim.lsp.buf.definition()<CR>')
  map('n','K','<cmd>lua vim.lsp.buf.hover()<CR>')
  map('n','<leader>gr','<cmd>lua vim.lsp.buf.references()<CR>')
  map('n','<leader>gs','<cmd>lua vim.lsp.buf.signature_help()<CR>')
  map('n','<leader>gi','<cmd>lua vim.lsp.buf.implementation()<CR>')
  map('n','<leader>gt','<cmd>lua vim.lsp.buf.type_definition()<CR>')
  map('n','<leader>gw','<cmd>lua vim.lsp.buf.document_symbol()<CR>')
  map('n','<leader>gW','<cmd>lua vim.lsp.buf.workspace_symbol()<CR>')
  map('n','<leader>ah','<cmd>lua vim.lsp.buf.hover()<CR>')
  map('n','<leader>af','<cmd>lua vim.lsp.buf.code_action()<CR>')
  map('n','<leader>ar','<cmd>lua vim.lsp.buf.rename()<CR>')
  map('n','<leader>a=', '<cmd>lua vim.lsp.buf.formatting()<CR>')
  map('n','<leader>ai','<cmd>lua vim.lsp.buf.incoming_calls()<CR>')
  map('n','<leader>ao','<cmd>lua vim.lsp.buf.outgoing_calls()<CR>')
 -- vim.api.nvim_buf_set_keymap(bufnr, 'n', 'pd', '<cmd>lua peek_definition()<CR>', opts)
  -- map('n', '<leader>dp', ':PrevDiagnostic<CR>')
  -- map('n', '<leader>dn', ':NextDiagnostic<CR>')
  map('n', '<leader>dp', '<cmd>lua vim.lsp.diagnostic.goto_prev {wrap=false}<CR>')
  map('n', '<leader>dn', '<cmd>lua vim.lsp.diagnostic.goto_next {wrap=false}<CR>')
  map('n', '<leader>Dp', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>')
  map('n', '<leader>Dn', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>')
end

local servers = { 'ccls', 'bashls', 'pyls', 'vimls', 'yamlls', 'sumneko_lua', 'rust_analyzer' }
for _, lsp in ipairs(servers) do
  lsp_status.register_progress()
  lsp_status.config({
    status_symbol = '',
    indicator_errors = 'e',
    indicator_warnings = 'w',
    indicator_info = 'i',
    indicator_hint = 'h',
    indicator_ok = '✔️',
--    spinner_frames = { '⣾', '⣽', '⣻', '⢿', '⡿', '⣟', '⣯', '⣷' },
  })
  if lsp == "pyls" then
    nvim_lsp[lsp].setup {
      on_attach = on_attach,
      capabilities = lsp_status.capabilities,
      settings = {
        pyls = {
          plugins = {
            pycodestyle = {
              maxLineLength = 120,
            },
            mccabe = {
              threshold = 30,
            },
          },
        },
      },

    }
  elseif lsp == "sumneko_lua" then
    nvim_lsp[lsp].setup {
      on_attach=on_attach,
      cmd = {"/usr/bin/lua-language-server", "-E"},
      settings = {
        Lua = {
          runtime = { version = "LuaJIT", path = vim.split(package.path, ';'), },
          completion = { keywordSnippet = "Disable", },
          diagnostics = { enable = true, globals = {
            "vim", "describe", "it", "before_each", "after_each" },
          },
          workspace = {
            library = {
              [vim.fn.expand("$VIMRUNTIME/lua")] = true,
              [vim.fn.expand("$VIMRUNTIME/lua/vim/lsp")] = true,
            }
          }
        }
      }
    }
  elseif lsp == "ccls" then
    nvim_lsp[lsp].setup{
      on_attach = on_attach;
      capabilities = lsp_status.capabilities;
      init_options = {
        cache = {directory = "/tmp/ccls-cache",};
        compilationDatabaseDirectory = "build";
        index = {
          threads = 0;
        };
      }
    }
  else
    nvim_lsp[lsp].setup {
      on_attach = on_attach,
      capabilities = lsp_status.capabilities,
    }
  end
end
