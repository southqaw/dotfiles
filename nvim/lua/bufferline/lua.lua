require 'bufferline'.setup{
  options = {
    view = "multiwindow",
    numbers = "ordinal",
    number_style = "superscript",
    mappings = true,
    buffer_close_icon= '',
    modified_icon = '●',
    close_icon = '',
    left_trunc_marker = '',
    right_trunc_marker = '',
    max_name_length = 18,
    max_prefix_length = 15, -- prefix used when a buffer is deduplicated
    tab_size = 18,
    diagnostics = "nvim_lsp",
    diagnostics_indicator = function(count, level, diagnostics_dict)
      local s = " "
      for e, n in pairs(diagnostics_dict) do
        local sym = e == "error" and " "
        or (e == "warning" and " " or "" )
        s = s .. n .. sym
      end
      return s
    end,
    show_buffer_close_icons = false,
    show_close_icon = true,
    show_tab_indicators = true,
    persist_buffer_sort = true, -- whether or not custom sorted buffers should persist
    -- can also be a table containing 2 custom separators
    -- [focused and unfocused]. eg: { '|', '|' }
    separator_style = "thin",
    enforce_regular_tabs = false,
    always_show_bufferline = true,
  },
  highlights = {
    background = {
      guifg = "#586e75",
      guibg = "#002b36"
    },
    fill = {
      guifg = "#586e75",
      guibg = "#002b36"
    },
    buffer_selected = {
      guifg = "#93a1a1",
      guibg = "#073642",
      gui = "bold"
    },
    buffer_visible = {
      guifg = "#586e75",
      guibg = "#002b36"
    },
    separator_visible = {
      guifg = "#002b36",
      guibg = "#002b36"
    },
    separator_selected = {
      guifg = "#002b36",
      guibg = "#002b36"
    },
    separator = {
      guifg = "#002b36",
      guibg = "#002b36"
    },
    indicator_selected = {
      guifg = "#002b36",
      guibg = "#002b36"
    },
    modified_selected = {
      guifg = "#268bd2",
      guibg = "#073642"
    },
    diagnostic_selected = {
      guifg = "#93a1a1",
      guibg = "#073642"
    },
    info = {
      guifg = "#586e75",
      guisp = "#586e75",
      guibg = "#002b36"
    },
    info_visible = {
      guifg = "#2aa198",
      guibg = "#002b36"
    },
    info_selected = {
      guifg = "#2aa198",
      guibg = "#073642",
      gui = "bold,italic",
      guisp = "#2aa198"
    },
    info_diagnostic = {
      guifg = "#2aa198",
      guisp = "#586e75",
      guibg = "#002b36"
    },
    info_diagnostic_visible = {
      guifg = "#2aa198",
      guibg = "#002b36"
    },
    info_diagnostic_selected = {
      guifg = "#2aa198",
      guibg = "#073642",
      gui = "bold,italic",
      guisp = "#2aa198"
    },
    warning = {
      guifg = "#586e75",
      guisp = "#586e75",
      guibg = "#002b36"
    },
    warning_visible = {
      guifg = "#b58900",
      guibg = "#002b36"
    },
    warning_selected = {
      guifg = "#b58900",
      guibg = "#073642",
      gui = "bold,italic",
      guisp = "#b58900"
    },
    warning_diagnostic = {
      guifg = "#b58900",
      guisp = "#586e75",
      guibg = "#002b36"
    },
    warning_diagnostic_visible = {
      guifg = "#b58900",
      guibg = "#002b36"
    },
    warning_diagnostic_selected = {
      guifg = "#b58900",
      guibg = "#073642",
      gui = "bold,italic",
      guisp = "#b58900"
    },
    error = {
      guifg = "#586e75",
      guibg = "#002b36",
      guisp = "#586e75"
    },
    error_visible = {
      guifg = "#dc322f",
      guibg = "#002b36"
    },
    error_selected = {
      guifg = "#dc322f",
      guibg = "#073642",
      gui = "bold,italic",
      guisp = "#dc322f"
    },
    error_diagnostic = {
      guifg = "#dc322f",
      guibg = "#002b36",
      guisp = "#586e75"
    },
    error_diagnostic_visible = {
      guifg = "#dc322f",
      guibg = "#002b36"
    },
    error_diagnostic_selected = {
      guifg = "#dc322f",
      guibg = "#073642",
      gui = "bold,italic",
      guisp = "#dc322f"
    },
  }
}
