" Save
nnoremap <leader>w <Esc>:w<CR>
" Search and Replace
nnoremap <leader>c. :%s//g<Left><Left>
nnoremap <leader>c, :%s/\<<C-r><C-w>\>//g<Left><Left>
" Quit
"nnoremap <leader>x  <Esc>:x<CR>
nnoremap <leader>q  <Esc>:q<CR>
nnoremap <leader>Q  <Esc>:qa<CR>
" Navigate buffers
nnoremap <leader>bp :bprevious<CR>
nnoremap <leader>bn :bnext<CR>
nnoremap <leader>bf :bfirst<CR>
nnoremap <leader>bl :blast<CR>
nnoremap <leader>bd :bdelete<CR>
" Reload buffer
nnoremap <leader>e :e<CR>
nnoremap <leader>E :bufdo e<CR>
" Tab navigation
"nnoremap <leader>tp :tabprevious<CR>
"nnoremap <leader>tn :tabnext<CR>
"nnoremap <leader>tf :tabfirst<CR>
"nnoremap <leader>tl :tablast<CR>
"nnoremap <leader>tN :tabnew<CR>

nnoremap <leader>t :NERDTreeToggle<CR>
nnoremap <leader>T :ToggleNERDTreeAndTagbar<CR>

" Use <Tab> and <S-Tab> to navigate through popup menu
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
