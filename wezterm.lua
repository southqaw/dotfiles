local wezterm = require("wezterm")

-- The filled in variant of the < symbol
local SOLID_LEFT_ARROW = utf8.char(0xe0b2)

-- The filled in variant of the > symbol
local SOLID_RIGHT_ARROW = utf8.char(0xe0b0)

wezterm.on("format-tab-title", function(tab, tabs, panes, config, hover, max_width)
	local edge_background = "#181926"
	local background = "#24273a"
	local foreground = "#a5adcb"

	if tab.is_active then
		background = "#494d64"
		foreground = "#a5adcb"
	elseif hover then
		background = "#6e738d"
		foreground = "#a5adcb"
	end

	local edge_foreground = background

	-- ensure that the titles fit in the available space,
	-- and that we have room for the edges.
	local title = wezterm.truncate_right(tab.active_pane.title, max_width - 2)

	return {
		{ Background = { Color = edge_background } },
		{ Foreground = { Color = edge_foreground } },
		{ Text = SOLID_LEFT_ARROW },
		{ Background = { Color = background } },
		{ Foreground = { Color = foreground } },
		{ Text = title },
		{ Background = { Color = edge_background } },
		{ Foreground = { Color = edge_foreground } },
		{ Text = SOLID_RIGHT_ARROW },
	}
end)

return {
	-- font = wezterm.font_with_fallback({ "Fira Code", "Symbols Nerd Font", "Noto Color Emoji" }),
	font = wezterm.font_with_fallback({ "Fira Code Nerd Font", "Font Awesome 6 Free", "Font Awesome 6 Brands" }),
	font_size = 10,
	color_scheme = "Catppuccin Macchiato",
	use_fancy_tab_bar = false,
	default_prog = { "/usr/bin/fish", "-l" },
	hide_tab_bar_if_only_one_tab = true,
	tab_max_width = 32,
	window_background_opacity = 0.95,
	enable_wayland = true,
	keys = {
		{
			key = ")",
			mods = "CTRL|SHIFT",
			action = wezterm.action.SplitHorizontal({ domain = "CurrentPaneDomain" }),
		},
		{
			key = "(",
			mods = "CTRL|SHIFT",
			action = wezterm.action.SplitVertical({ domain = "CurrentPaneDomain" }),
		},
		{ key = "a", mods = "CTRL", action = wezterm.action.DisableDefaultAssignment },
		--		{
		--			key = "a",
		--			mods = "CTRL",
		--			action = wezterm.action_callback(function(window, pane)
		--				if pane:is_alt_screen_active() then
		--					-- allow "full screen" TUI apps to receive and handle CTRL-C for themselves
		--					window:perform_action(wezterm.action.SendKey({ key = "a", mods = "CTRL" }))
		--				end
		--			end),
		--		},
	},
	audible_bell = "Disabled",
	visual_bell = {
		fade_in_duration_ms = 150,
		fade_out_duration_ms = 150,
		target = "CursorColor",
	},
}
