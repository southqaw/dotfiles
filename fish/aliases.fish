# Global aliases
#alias c 'clear'     # Clear screen
#alias - 'cd -'      # Last folder
alias sl 'ls'
alias md 'mkdir'    # Make directory
alias bc 'eva'   # No welcome message, improved functions
alias iftop 'iftop -B'  # Measure in Bytes instead of Bits
alias ping 'ping -c3'   # Default to 3 attemps instead of unlimited
alias :e 'vim'      # Launch vim with ":e"
alias :q 'exit'     # exit with vim command
alias crontab 'crontab -i'  # No accidental removals
alias chmox 'chmod +x'
alias mount 'mount | column -t'
alias ack 'ack --color --color-filename=magenta --color-lineno=green --color-match=yellow'
# alias ls 'ls --color=auto --group-directories-first'
alias ls 'exa --group --header --group-directories-first'
alias grep 'grep --color=auto'
alias vim 'nvim'
alias cat 'bat'

function please
    eval sudo $history[1]
end

function bash
    /usr/bin/env bash --login
end

function su
    sudo /bin/su --shell=/usr/bin/fish
end

function vimall
    set FILES ./*
    vim $FILES
end

function mk
    mkdir $argv; and cd $argv
end

function cs
    cd $argv; and ls
end

# function tmux
#     set TEMP_TERM $TERM
#     set TERM xterm-256color
#     command tmux -2 $argv
#     set TERM $TEMP_TERM
# end
