function qrcode --description "Display a QRCode of the given text"
    qrencode $argv[1] -l Q -d 300 -s 5 -m 10 -o - | feh --title $argv[1] - &
end
