function venv -d "Create a new python VENV"
    set r (set_color red)
    set m (set_color magenta)
    set y (set_color yellow)
    set g (set_color green)
    set b (set_color blue)
    set c (set_color cyan)
    set n (set_color normal)


    if type -q deactivate
        echo
        echo $m"Deactivating"$n" VENV in "$y"$VIRTUAL_ENV"$n
        deactivate
        return
    end
    for dir in .env .venv env venv
        if test -d ./$dir
            echo
            echo $b"Activating"$n" VENV in "$y"$dir"$n
            source $dir/bin/activate.fish
            return
        end
    end
    echo
    echo $g"Creating"$n" new VENV in "$y".venv"$n
    python -m venv .venv
    source .venv/bin/activate.fish
    pip install --upgrade pip &> /dev/null
    if test -e requirements.txt
        pip install -r requirements.txt &> /dev/null
    end
end
