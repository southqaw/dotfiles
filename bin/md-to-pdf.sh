#!/usr/bin/env bash

# Converts a Github Markdown file to PDF
# requires:
#     pandoc
#     pdflatex

# Example
# pandoc --from=gfm --to=pdf -o OUT.pdf IN.md -V geometry:margin=1in --highlight-style tango -V colorlinks=true -V linkcolor=green -V urlcolor=blue -V toccolor=gray

INFILE=$1

if [[ ! -f ${INFILE} ]]; then
	echo "Invalid filename"
	exit 1
fi

# Some bash parameter expansion magic
INFILE=$(basename -- "$INFILE")
INFILEEXT="${INFILE##*.}"
INFILENAME="${INFILE%.*}"

if [[ $INFILEEXT != "md" ]]; then
	echo "Warning: input file doesn't appear to be Markdown"
fi

pandoc \
	--from=gfm \
	--to=pdf \
	-o "${INFILENAME}".pdf \
	"${INFILE}" \
	--highlight-style tango \
	-V geometry:margin=1in \
	-V colorlinks=true \
	-V linkcolor=green \
	-V urlcolor=blue \
	-V toccolor=gray
