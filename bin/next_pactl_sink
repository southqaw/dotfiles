#!/usr/bin/env bash

cleanup(){
    echo "$1"
    IFS=$OLD_IFS
    exit 0
}

OLD_IFS=$IFS
IFS=$'\n'

### Determine if any audio streams exist
INPUTS_RAW=($(pactl list short sink-inputs))
if [[ -z "INPUTS_RAW" ]]; then
    cleanup "Inputs Raw"
fi

### Determine current audio sink
SINKS_RAW=($(pactl list short sinks))
NUM_SINKS=${#SINKS_RAW[@]}
CURRENT_SINK=-1
CURRENT_SINK_INDEX=0
for i in "${SINKS_RAW[@]}"; do
    grep "RUNNING" <<< $i
    if [[ $? -eq 0 ]]; then
        CURRENT_SINK=$(cut -d $'\t' -f1 <<< $i)
        break
    fi
    CURRENT_SINK_INDEX=$(($CURRENT_SINK_INDEX+1))
done
if [[ $CURRENT_SINK -eq -1 ]]; then
    cleanup "No Running"
fi

### Get next sink
if [[ $(($CURRENT_SINK_INDEX+1)) -lt $NUM_SINKS ]]; then
    NEW_SINK_INDEX=$(($CURRENT_SINK_INDEX+1))
else
    NEW_SINK_INDEX=0
fi
NEW_SINK=$(cut -d $'\t' -f1 <<< ${SINKS_RAW[$NEW_SINK_INDEX]})

### Set all audio streams to new sink
for i in "${INPUTS_RAW[@]}"; do
    STREAM_ID=$(cut -d $'\t' -f1 <<< $i)
    pactl move-sink-input "$STREAM_ID" "$NEW_SINK"
done

### Clean up
cleanup "Done"
