local plugins = {
  {
    "williamboman/mason.nvim",
    opts = {
      ensure_installed = {
        "rust-analyzer",
        "codelldb",
        "stylua",
        "shellcheck",
        "shfmt",
        "clangd",
        "bash-language-server",
        "black",
        "clang-format",
        "json-lsp",
        "lua-language-server",
        "yaml-language-server",
        "yamlfmt",
        "yamllint",
        "ruff",
        "ruff-lsp",
        "pyright",
        "flake8",
      },
    },
  },
  {
    "simrat39/symbols-outline.nvim",
    cmd = "SymbolsOutline",
    keys = { { "<leader>cs", "<cmd>SymbolsOutline<cr>", desc = "Symbols Outline" } },
    opts = {
      -- add your options that should be passed to the setup() function here
      position = "right",
    },
  },
  {
    "folke/trouble.nvim",
    -- opts will be merged with the parent spec
    dependencies = { "nvim-tree/nvim-web-devicons" },
    opts = { use_diagnostic_signs = true },
  },
  {
    "neovim/nvim-lspconfig",
    dependencies = {
      "jose-elias-alvarez/null-ls.nvim",
      config = function()
        require "custom.configs.null-ls"
      end,
    },
    config = function()
      require "plugins.configs.lspconfig"
      require "custom.configs.lspconfig"
    end,
  },
  {
    "rust-lang/rust.vim",
    ft = "rust",
    init = function()
      vim.g.rustfmt_autosave = 1
    end,
  },
  {
    "simrat39/rust-tools.nvim",
    ft = "rust",
    dependencies = "neovim/nvim-lspconfig",
    opts = function()
      return require "custom.configs.rust-tools"
    end,
    config = function(_, opts)
      require("rust-tools").setup(opts)
      require("core.utils").load_mappings "rust"
    end,
  },
  {
    "rcarriga/nvim-dap-ui",
    dependencies = "mfussenegger/nvim-dap",
    init = function()
      require "custom.configs.dap"
    end,
  },
  {
    "saecki/crates.nvim",
    ft = { "rust", "toml" },
    config = function(_, opts)
      local crates = require "crates"
      crates.setup(opts)
      crates.show()
    end,
  },
  {
    "hrsh7th/nvim-cmp",
    opts = function()
      local M = require "plugins.configs.cmp"
      table.insert(M.sources, { name = "crates" })
      return M
    end,
  },
  {
    "folke/which-key.nvim",
    config = function()
      -- require("plugins.config.whichkey")
      local present, wk = pcall(require, "which-key")
      if not present then
        return
      end
      wk.register {
        ["<leader>"] = {
          c = { name = "+code" },
          d = { name = "+dap" },
          f = {
            function()
              vim.diagnostic.open_float { border = "rounded" }
            end,
            "+files",
          },
          g = { name = "+git" },
          l = { name = "+live server" },
          p = { name = "+terms" },
          r = { name = "+refactor" },
          t = { name = "+telescope" },
          w = { name = "+which-key/workspace" },
          m = { name = "+marks" },
        },
      }
    end,
    setup = function()
      require("core.utils").load_mappings "whichkey"
    end,
  },
  {
    "nvim-neorg/neorg",
    ft = "norg",
    build = ":Neorg sync-parsers",
    opts = {
      load = {
        ["core.defaults"] = {}, -- Loads default behaviour
        ["core.keybinds"] = {
          config = {
            default_keybinds = true,
            hook = function(keybinds)
              keybinds.remap_key("norg", "n", "gtd", "<Leader>nd")
              keybinds.remap_key("norg", "n", "gtu", "<Leader>nu")
              keybinds.remap_key("norg", "n", "gtp", "<Leader>np")
              keybinds.remap_key("norg", "n", "gth", "<Leader>nh")
              keybinds.remap_key("norg", "n", "gtc", "<Leader>nc")
              keybinds.remap_key("norg", "n", "gtr", "<Leader>nr")
              keybinds.remap_key("norg", "n", "gti", "<Leader>ni")
              keybinds.remap_key("norg", "n", "gF", "<Leader>nF")
              keybinds.remap_key("norg", "n", "gO", "<Leader>nO")
              keybinds.unmap("norg", "n", "gd")
            end,
          },
        },
        ["core.completion"] = {
          config = {
            engine = "nvim-cmp",
            name = "neorg",
          },
        }, -- Loads default behaviour
        ["core.concealer"] = {}, -- Adds pretty icons to your documents
        ["core.dirman"] = { -- Manages Neorg workspaces
          config = {
            workspaces = {
              notes = "~/Notes",
            },
          },
        },
        ["core.export"] = {},
        ["core.export.markdown"] = {},
      },
    },
    dependencies = { { "nvim-lua/plenary.nvim" } },
  },

  {
    "hrsh7th/nvim-cmp",
    opts = function(_, opts)
      local cmp = require "cmp"
      opts.sources = cmp.config.sources(vim.list_extend(opts.sources, { { name = "neorg" } }))
    end,
  },
}
return plugins
