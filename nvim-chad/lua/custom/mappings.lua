local M = {}
M.ui = {
  n = {
    ["<leader>tt"] = {
      function()
        require("base46").toggle_transparency()
      end,
      "toggle transparency",
    },
  },
}
M.dap = {
  n = {
    ["<leader>db"] = {
      "<cmd> DapToggleBreakpoint <CR>",
      "Toggle breakpoint",
    },
    ["<leader>dus"] = {
      function()
        local widgets = require "dap.ui.widgets"
        local sidebar = widgets.sidebar(widgets.scopes)
        sidebar.open()
      end,
      "Open debuggind sidebar",
    },
  },
}
M.crates = {
  n = {
    ["<leader>rcu"] = {
      function()
        require("crates").upgrade_all_crates()
      end,
      "Update crates",
    },
  },
}
M.rust = {
  plugin = true,
  n = {
    ["<leader>ck"] = {
      "<cmd>RustHoverActions<cr>",
      "Hover Actions (Rust)",
    },
    ["<leader>ca"] = {
      "<cmd>RustCodeAction<cr>",
      "Code Actions (Rust)",
    },
  },
}

M.trouble = {
  n = {
    ["<leader>cx"] = {"<cmd>TroubleToggle<cr>", "Toggle Trouble"},
    ["<leader>cw"] = {"<cmd>TroubleToggle workspace_diagnostics<cr>", "Workspace Diagnostics"},
    ["<leader>cd"] = {"<cmd>TroubleToggle document_diagnostics<cr>", "Document Diagnostics"},
    ["<leader>cl"] = {"<cmd>TroubleToggle loclist<cr>", "Loc List"},
    ["<leader>cq"] = {"<cmd>TroubleToggle quickfix<cr>", "Quickfix"},
    ["gR"] = {"<cmd>TroubleToggle lsp_references<cr>", "LSP References (Trouble)"},
  }
}

return M
