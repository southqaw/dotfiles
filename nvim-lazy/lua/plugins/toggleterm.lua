return {
  {
    "akinsho/toggleterm.nvim",
    version = "*",
    opts = {
      direction = "float",
    },
    config = true,
    keys = {
      {
        "<leader>tt",
        "<cmd>ToggleTerm<cr>",
        desc = "Open a floating terminal in the current directory",
      },
    },
  },
}
