return {
  {
    "m-pilia/vim-pkgbuild",
    lazy = false,
    init = function() end,
  },
  {
    "peterhoeg/vim-qml",
    lazy = false,
    init = function() end,
  },
  {
    "machakann/vim-swap",
    lazy = false,
    init = function() end,
  },
  {
    "lambdalisue/vim-suda",
    lazy = false,
    init = function() end,
  },
}
