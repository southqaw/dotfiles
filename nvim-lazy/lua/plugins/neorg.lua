return {
  {
    "vhyrro/luarocks.nvim",
    priority = 1000, -- We'd like this plugin to load first out of the rest
    config = true, -- This automatically runs `require("luarocks-nvim").setup()`
  },
  {
    "nvim-neorg/neorg",
    dependencies = { "luarocks.nvim" },
    lazy = false,
    version = "*",
    config = true,
    opts = {
      load = {
        ["core.defaults"] = {}, -- Loads default behaviour
        ["core.keybinds"] = {
          config = {
            default_keybinds = true,
            -- hook = function(keybinds)
            --   keybinds.remap_key("norg", "n", "gtd", "<Leader>nd")
            --   keybinds.remap_key("norg", "n", "gtu", "<Leader>nu")
            --   keybinds.remap_key("norg", "n", "gtp", "<Leader>np")
            --   keybinds.remap_key("norg", "n", "gth", "<Leader>nh")
            --   keybinds.remap_key("norg", "n", "gtc", "<Leader>nc")
            --   keybinds.remap_key("norg", "n", "gtr", "<Leader>nr")
            --   keybinds.remap_key("norg", "n", "gti", "<Leader>ni")
            --   keybinds.remap_key("norg", "n", "gF", "<Leader>nF")
            --   keybinds.remap_key("norg", "n", "gO", "<Leader>nO")
            --   keybinds.unmap("norg", "n", "gd")
            -- end,
          },
        },
        ["core.completion"] = {
          config = {
            engine = "nvim-cmp",
            name = "neorg",
          },
        }, -- Loads default behaviour
        ["core.concealer"] = {}, -- Adds pretty icons to your documents
        ["core.dirman"] = { -- Manages Neorg workspaces
          config = {
            workspaces = {
              notes = "~/Notes",
              timesheets = "~/Notes/work/Timesheets",
              meetings = "~/Notes/work/Meetings",
              tasks = "~/Notes/work/Tasks",
            },
            default_workspace = "notes",
          },
        },
        ["core.export"] = {},
        ["core.export.markdown"] = {},
      },
    },
  },

  {
    "hrsh7th/nvim-cmp",
    opts = function(_, opts)
      local cmp = require("cmp")
      opts.sources = cmp.config.sources(vim.list_extend(opts.sources, { { name = "neorg" } }))
    end,
  },
}
