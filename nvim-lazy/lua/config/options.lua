-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here
--Indent settings
vim.o.tabstop = 4
vim.o.shiftwidth = 4
vim.o.softtabstop = 4
vim.o.expandtab = true
vim.o.smartindent = true
vim.o.smarttab = true

--Save undo history
vim.cmd([[set undofile]])

--Case insensitive searching UNLESS \C<search> or capital in search
vim.o.ignorecase = true
vim.o.smartcase = false

vim.opt.completeopt = "menu,menuone,noselect"
